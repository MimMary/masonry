function Masonry(className, settingsObj) {
  this.className = className;
  this.settingsObj = settingsObj;
  this.render();
  if(settingsObj.autoResize) {
    window.addEventListener("resize", (e) => {
      this.render();
    });
  }
}

Masonry.prototype.render = function() {
  let windWidth = window.innerWidth;
  let imageWidth = this.settingsObj.columnWidth;
  const element = document.querySelectorAll(
    `${this.className} li`
  );
  let array = [];
  let columnCount = Math.floor(windWidth / imageWidth);
  for (let i = 0; i < columnCount; ++i) {
    array[i] = 0;
  }
  for (let i = 0; i < element.length; i++) {
    let minHeight = Math.min(...array);
    let index = array.indexOf(minHeight);
    let top = minHeight;
    let left = index * imageWidth;
    element[i].style.width = `${imageWidth}px`;
    element[i].style.left = `${left}px`;
    element[i].style.top = `${top}px`;
    array[index] += element[i].offsetHeight;
  }
};


new Masonry(".masonry", {
  columnWidth: 400,
  autoResize: true,
});


